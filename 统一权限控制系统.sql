/*
Navicat MySQL Data Transfer

Source Server         : locahost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : gt

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-10-20 16:56:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门表',
  `name` varchar(200) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deptgrade` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('56', 'aaa', '0', '1', 'aaa', '2015-10-20 16:31:30', null);
INSERT INTO `dept` VALUES ('58', 'ææ¯é¨', '0', '1', 'ææ¯é¨', '2015-10-20 16:48:33', null);

-- ----------------------------
-- Table structure for ly_buttom
-- ----------------------------
DROP TABLE IF EXISTS `ly_buttom`;
CREATE TABLE `ly_buttom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `buttom` varchar(200) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_buttom
-- ----------------------------
INSERT INTO `ly_buttom` VALUES ('1', '新增', '<button type=\"button\" id=\"addFun\" class=\"btn btn-primary marR10\">新增</button>', '');
INSERT INTO `ly_buttom` VALUES ('2', '编辑', '<button type=\"button\" id=\"editFun\" class=\"btn btn-info marR10\">编辑</button>', null);
INSERT INTO `ly_buttom` VALUES ('3', '删除', '<button type=\"button\" id=\"delFun\" class=\"btn btn-danger marR10\">删除</button>', null);
INSERT INTO `ly_buttom` VALUES ('4', '上传', '<button type=\"button\" id=\"upLoad\" class=\"btn btn-primary marR10\">上传</button>', null);
INSERT INTO `ly_buttom` VALUES ('5', '下载', '<button type=\"button\" id=\"downLoad\" class=\"btn btn-primary marR10\">下载</button>', null);
INSERT INTO `ly_buttom` VALUES ('6', '上移', '<button type=\"button\" id=\"lyGridUp\" class=\"btn btn-success marR10\">上移</button>', null);
INSERT INTO `ly_buttom` VALUES ('7', '下移', '<button type=\"button\" id=\"lyGridDown\" class=\"btn btn btn-grey marR10\">下移</button>', null);
INSERT INTO `ly_buttom` VALUES ('8', '分配权限', '<button type=\"button\" id=\"permissions\" class=\"btn btn btn-grey marR10\">分配权限</button>', null);

-- ----------------------------
-- Table structure for ly_log
-- ----------------------------
DROP TABLE IF EXISTS `ly_log`;
CREATE TABLE `ly_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `methods` varchar(30) DEFAULT NULL,
  `actionTime` varchar(30) DEFAULT NULL,
  `userIP` varchar(30) DEFAULT NULL,
  `operTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_log
-- ----------------------------
INSERT INTO `ly_log` VALUES ('135', 'admin', '系统管理', '用户管理-新增用户', '7', '0:0:0:0:0:0:0:1', '2015-09-14 19:25:27', '执行成功!');
INSERT INTO `ly_log` VALUES ('136', 'admin', '系统管理', '用户管理-新增用户', '41', '192.168.20.69', '2015-09-15 09:59:04', '执行成功!');
INSERT INTO `ly_log` VALUES ('137', 'admin', '系统管理', '用户管理-修改用户', '18', '192.168.20.69', '2015-09-15 09:59:15', '执行成功!');
INSERT INTO `ly_log` VALUES ('138', 'admin', '系统管理', '用户管理-新增用户', '2', '192.168.20.72', '2015-09-15 11:44:39', '执行成功!');
INSERT INTO `ly_log` VALUES ('139', 'admin', '系统管理', '用户管理/组管理-修改权限', '30', '192.168.20.72', '2015-09-15 11:45:22', '执行成功!');
INSERT INTO `ly_log` VALUES ('140', 'admin', '系统管理', '用户管理/组管理-修改权限', '2', '192.168.20.72', '2015-09-15 11:46:55', '执行成功!');
INSERT INTO `ly_log` VALUES ('141', 'admin', '系统管理', '用户管理-修改用户', '17', '192.168.20.72', '2015-09-15 15:08:08', '执行成功!');
INSERT INTO `ly_log` VALUES ('142', 'admin', '系统管理', '资源管理-新增资源', '18', '0:0:0:0:0:0:0:1', '2015-09-24 15:39:00', '执行成功!');
INSERT INTO `ly_log` VALUES ('143', 'admin', '系统管理', '资源管理-新增资源', '51', '0:0:0:0:0:0:0:1', '2015-09-24 15:39:25', '执行成功!');
INSERT INTO `ly_log` VALUES ('144', 'admin', '系统管理', '资源管理-删除资源', '141', '0:0:0:0:0:0:0:1', '2015-09-24 15:39:46', '执行成功!');
INSERT INTO `ly_log` VALUES ('145', 'admin', '系统管理', '用户管理-修改用户', '9', '0:0:0:0:0:0:0:1', '2015-09-25 09:33:23', '执行成功!');
INSERT INTO `ly_log` VALUES ('146', 'admin', '系统管理', '用户管理-修改用户', '39', '0:0:0:0:0:0:0:1', '2015-10-08 09:17:41', '执行成功!');
INSERT INTO `ly_log` VALUES ('147', 'admin', '系统管理', '用户管理-删除用户', '34', '127.0.0.1', '2015-10-13 09:18:13', '执行成功!');
INSERT INTO `ly_log` VALUES ('148', 'admin', '系统管理', '资源管理-新增资源', '3', '127.0.0.1', '2015-10-19 14:30:16', '执行成功!');
INSERT INTO `ly_log` VALUES ('149', 'admin', '系统管理', '用户管理/组管理-修改权限', '50', '127.0.0.1', '2015-10-19 14:31:04', '执行成功!');
INSERT INTO `ly_log` VALUES ('150', 'admin', '系统管理', '资源管理-修改资源', '4', '127.0.0.1', '2015-10-19 14:37:57', '执行成功!');
INSERT INTO `ly_log` VALUES ('151', 'admin', '系统管理', '资源管理-新增资源', '2', '127.0.0.1', '2015-10-19 14:41:41', '执行成功!');
INSERT INTO `ly_log` VALUES ('152', 'admin', '系统管理', '用户管理/组管理-修改权限', '6', '127.0.0.1', '2015-10-19 16:01:34', '执行成功!');
INSERT INTO `ly_log` VALUES ('153', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-19 16:40:12', '执行成功!');
INSERT INTO `ly_log` VALUES ('154', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-19 16:52:58', '执行成功!');
INSERT INTO `ly_log` VALUES ('155', 'admin', '系统管理', '部门管理-新增部门', '26064', '127.0.0.1', '2015-10-19 16:58:51', '执行成功!');
INSERT INTO `ly_log` VALUES ('156', 'admin', '系统管理', '资源管理-新增资源', '2', '127.0.0.1', '2015-10-19 16:59:44', '执行成功!');
INSERT INTO `ly_log` VALUES ('157', 'admin', '系统管理', '资源管理-删除资源', '45', '127.0.0.1', '2015-10-19 16:59:51', '执行成功!');
INSERT INTO `ly_log` VALUES ('158', 'admin', '系统管理', '资源管理-新增资源', '3', '127.0.0.1', '2015-10-19 17:00:06', '执行成功!');
INSERT INTO `ly_log` VALUES ('159', 'admin', '系统管理', '资源管理-删除资源', '41', '127.0.0.1', '2015-10-19 17:00:30', '执行成功!');
INSERT INTO `ly_log` VALUES ('160', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-19 17:26:26', '执行成功!');
INSERT INTO `ly_log` VALUES ('161', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-19 17:33:31', '执行成功!');
INSERT INTO `ly_log` VALUES ('162', 'admin', '系统管理', '部门管理-新增部门', '78095', '127.0.0.1', '2015-10-20 08:28:22', '执行成功!');
INSERT INTO `ly_log` VALUES ('163', 'admin', '系统管理', '部门管理-新增部门', '154222', '127.0.0.1', '2015-10-20 09:48:54', '执行成功!');
INSERT INTO `ly_log` VALUES ('164', 'admin', '系统管理', '部门管理-新增部门', '6270', '127.0.0.1', '2015-10-20 10:01:29', '执行成功!');
INSERT INTO `ly_log` VALUES ('165', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-20 11:42:30', '执行成功!');
INSERT INTO `ly_log` VALUES ('166', 'admin', '系统管理', '部门管理-新增部门', '40', '127.0.0.1', '2015-10-20 16:02:28', '执行成功!');
INSERT INTO `ly_log` VALUES ('167', 'admin', '系统管理', '部门管理-新增部门', '9955', '127.0.0.1', '2015-10-20 16:03:04', '执行成功!');
INSERT INTO `ly_log` VALUES ('168', 'admin', '系统管理', '部门管理-新增部门', '6989', '127.0.0.1', '2015-10-20 16:31:30', '执行成功!');
INSERT INTO `ly_log` VALUES ('169', 'admin', '系统管理', '部门管理-新增部门', '109', '127.0.0.1', '2015-10-20 16:32:27', '执行成功!');
INSERT INTO `ly_log` VALUES ('170', 'admin', '系统管理', '部门管理-新增部门', '4', '127.0.0.1', '2015-10-20 16:48:33', '执行成功!');

-- ----------------------------
-- Table structure for ly_resources
-- ----------------------------
DROP TABLE IF EXISTS `ly_resources`;
CREATE TABLE `ly_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `resKey` varchar(50) DEFAULT NULL,
  `type` varchar(40) DEFAULT NULL,
  `resUrl` varchar(200) DEFAULT NULL,
  `level` int(4) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `ishide` int(3) DEFAULT '0',
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `resKey` (`resKey`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_resources
-- ----------------------------
INSERT INTO `ly_resources` VALUES ('1', '系统基础管理', '0', 'system', '0', 'system', '1', 'fa-desktop', '0', '系统基础管理');
INSERT INTO `ly_resources` VALUES ('2', '用户管理', '1', 'account', '1', '/user/list.shtml', '2', null, '0', null);
INSERT INTO `ly_resources` VALUES ('3', '角色管理', '1', 'role', '1', '/role/list.shtml', '7', 'fa-list', '0', '组管理');
INSERT INTO `ly_resources` VALUES ('4', '菜单管理', '1', 'ly_resources', '1', '/resources/list.shtml', '12', 'fa-list-alt', '0', '菜单管理');
INSERT INTO `ly_resources` VALUES ('5', '新增用户', '2', 'account_add', '2', '/user/addUI.shtml', '3', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;addAccount&quot;&nbsp;class=&quot;btn&nbsp;btn-primary&nbsp;marR10&quot;&gt;新增&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('6', '修改用户', '2', 'account_edit', '2', '/user/editUI.shtml', '4', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;editAccount&quot;&nbsp;class=&quot;btn&nbsp;btn-info&nbsp;marR10&quot;&gt;编辑&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('7', '删除用户', '2', 'account_delete', '2', '/user/deleteById.shtml', '5', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;delAccount&quot;&nbsp;class=&quot;btn&nbsp;btn-danger&nbsp;marR10&quot;&gt;删除&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('8', '新增角色', '3', 'role_add', '2', '/role/addUI.shtml', '8', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;addRole&quot;&nbsp;class=&quot;btn&nbsp;btn-primary&nbsp;marR10&quot;&gt;新增&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('9', '修改角色', '3', 'role_edit', '2', '/role/editUI.shtml', '9', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;editRole&quot;&nbsp;class=&quot;btn&nbsp;btn-info&nbsp;marR10&quot;&gt;编辑&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('10', '删除角色', '3', 'role_delete', '2', '/role/delete.shtml', '10', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;delRole&quot;&nbsp;class=&quot;btn&nbsp;btn-danger&nbsp;marR10&quot;&gt;删除&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('11', '分配权限', '3', 'role_perss', '2', '/resources/permissions.shtml', '11', '无', '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;permissions&quot;&nbsp;class=&quot;btn&nbsp;btn&nbsp;btn-grey&nbsp;marR10&quot;&gt;分配权限&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('25', '登陆信息管理', '0', 'ly_login', '0', 'ly_login', '18', 'fa-calendar', '0', '登陆信息管理');
INSERT INTO `ly_resources` VALUES ('26', '用户登录记录', '25', 'ly_log_list', '1', '/userlogin/listUI.shtml', '19', null, '0', '用户登录记录');
INSERT INTO `ly_resources` VALUES ('27', '操作日志管理', '0', 'ly_logmain', '0', 'ly_log', '20', 'fa-picture-o', '1', '操作日志管理');
INSERT INTO `ly_resources` VALUES ('28', '日志查询', '27', 'ly_log', '1', '/log/list.shtml', '21', null, '0', null);
INSERT INTO `ly_resources` VALUES ('29', '新增菜单资源', '4', 'ly_resources_add', '2', '/resources/addUI.shtml', '13', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;addFun&quot;&nbsp;class=&quot;btn&nbsp;btn-primary&nbsp;marR10&quot;&gt;新增&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('30', '修改菜单资源', '4', 'ly_resources_edit', '2', '/resources/editUI.shtml', '14', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;editFun&quot;&nbsp;class=&quot;btn&nbsp;btn-info&nbsp;marR10&quot;&gt;编辑&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('31', '删除菜单资源', '4', 'ly_resources_delete', '2', '/resources/delete.shtml', '15', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;delFun&quot;&nbsp;class=&quot;btn&nbsp;btn-danger&nbsp;marR10&quot;&gt;删除&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('32', '系统监控管理', '0', 'monitor', '0', 'monitor', '16', 'fa-tag', '0', '系统监控管理');
INSERT INTO `ly_resources` VALUES ('33', '实时监控', '32', 'sysmonitor', '1', '/monitor/monitor.shtml', '17', null, '0', '实时监控');
INSERT INTO `ly_resources` VALUES ('34', '分配权限', '2', 'permissions', '2', '/resources/permissions.shtml', '6', null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;permissions&quot;&nbsp;class=&quot;btn&nbsp;btn&nbsp;btn-grey&nbsp;marR10&quot;&gt;分配权限&lt;/button&gt;');
INSERT INTO `ly_resources` VALUES ('35', '告警列表', '32', 'monitor_warn', '1', '/monitor/list.shtml', null, null, '0', '告警列表');
INSERT INTO `ly_resources` VALUES ('36', '客户管理列表', '1', 'employee_list', '1', '/employee/list', null, null, '0', '客户管理列表');
INSERT INTO `ly_resources` VALUES ('37', '部门管理', '1', 'dept', '1', '/dept/list.shtml', null, null, '0', null);
INSERT INTO `ly_resources` VALUES ('38', '新增部门', '37', 'dept_add', '2', '/dept/addUI.shtml', null, null, '0', '&lt;button&nbsp;type=&quot;button&quot;&nbsp;id=&quot;addFun&quot;&nbsp;class=&quot;btn&nbsp;btn-primary&nbsp;marR10&quot;&gt;新增&lt;/button&gt;');

-- ----------------------------
-- Table structure for ly_res_user
-- ----------------------------
DROP TABLE IF EXISTS `ly_res_user`;
CREATE TABLE `ly_res_user` (
  `resId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`resId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_res_user
-- ----------------------------
INSERT INTO `ly_res_user` VALUES ('1', '1');
INSERT INTO `ly_res_user` VALUES ('2', '1');
INSERT INTO `ly_res_user` VALUES ('3', '1');
INSERT INTO `ly_res_user` VALUES ('4', '1');
INSERT INTO `ly_res_user` VALUES ('5', '1');
INSERT INTO `ly_res_user` VALUES ('6', '1');
INSERT INTO `ly_res_user` VALUES ('7', '1');
INSERT INTO `ly_res_user` VALUES ('8', '1');
INSERT INTO `ly_res_user` VALUES ('9', '1');
INSERT INTO `ly_res_user` VALUES ('10', '1');
INSERT INTO `ly_res_user` VALUES ('11', '1');
INSERT INTO `ly_res_user` VALUES ('25', '1');
INSERT INTO `ly_res_user` VALUES ('26', '1');
INSERT INTO `ly_res_user` VALUES ('27', '1');
INSERT INTO `ly_res_user` VALUES ('28', '1');
INSERT INTO `ly_res_user` VALUES ('29', '1');
INSERT INTO `ly_res_user` VALUES ('30', '1');
INSERT INTO `ly_res_user` VALUES ('31', '1');
INSERT INTO `ly_res_user` VALUES ('32', '1');
INSERT INTO `ly_res_user` VALUES ('33', '1');
INSERT INTO `ly_res_user` VALUES ('34', '1');
INSERT INTO `ly_res_user` VALUES ('35', '1');
INSERT INTO `ly_res_user` VALUES ('36', '1');
INSERT INTO `ly_res_user` VALUES ('37', '1');
INSERT INTO `ly_res_user` VALUES ('1', '2');
INSERT INTO `ly_res_user` VALUES ('2', '2');
INSERT INTO `ly_res_user` VALUES ('5', '2');
INSERT INTO `ly_res_user` VALUES ('6', '2');
INSERT INTO `ly_res_user` VALUES ('7', '2');
INSERT INTO `ly_res_user` VALUES ('34', '2');
INSERT INTO `ly_res_user` VALUES ('1', '3');
INSERT INTO `ly_res_user` VALUES ('2', '3');
INSERT INTO `ly_res_user` VALUES ('3', '3');
INSERT INTO `ly_res_user` VALUES ('4', '3');
INSERT INTO `ly_res_user` VALUES ('5', '3');
INSERT INTO `ly_res_user` VALUES ('6', '3');
INSERT INTO `ly_res_user` VALUES ('7', '3');
INSERT INTO `ly_res_user` VALUES ('8', '3');
INSERT INTO `ly_res_user` VALUES ('9', '3');
INSERT INTO `ly_res_user` VALUES ('10', '3');
INSERT INTO `ly_res_user` VALUES ('11', '3');
INSERT INTO `ly_res_user` VALUES ('25', '3');
INSERT INTO `ly_res_user` VALUES ('26', '3');
INSERT INTO `ly_res_user` VALUES ('27', '3');
INSERT INTO `ly_res_user` VALUES ('28', '3');
INSERT INTO `ly_res_user` VALUES ('29', '3');
INSERT INTO `ly_res_user` VALUES ('30', '3');
INSERT INTO `ly_res_user` VALUES ('31', '3');
INSERT INTO `ly_res_user` VALUES ('32', '3');
INSERT INTO `ly_res_user` VALUES ('33', '3');
INSERT INTO `ly_res_user` VALUES ('34', '3');
INSERT INTO `ly_res_user` VALUES ('35', '3');
INSERT INTO `ly_res_user` VALUES ('36', '3');
INSERT INTO `ly_res_user` VALUES ('37', '3');
INSERT INTO `ly_res_user` VALUES ('38', '3');
INSERT INTO `ly_res_user` VALUES ('1', '4');
INSERT INTO `ly_res_user` VALUES ('2', '4');
INSERT INTO `ly_res_user` VALUES ('3', '4');
INSERT INTO `ly_res_user` VALUES ('4', '4');
INSERT INTO `ly_res_user` VALUES ('5', '4');
INSERT INTO `ly_res_user` VALUES ('6', '4');
INSERT INTO `ly_res_user` VALUES ('7', '4');
INSERT INTO `ly_res_user` VALUES ('8', '4');
INSERT INTO `ly_res_user` VALUES ('9', '4');
INSERT INTO `ly_res_user` VALUES ('10', '4');
INSERT INTO `ly_res_user` VALUES ('11', '4');
INSERT INTO `ly_res_user` VALUES ('25', '4');
INSERT INTO `ly_res_user` VALUES ('26', '4');
INSERT INTO `ly_res_user` VALUES ('27', '4');
INSERT INTO `ly_res_user` VALUES ('28', '4');
INSERT INTO `ly_res_user` VALUES ('29', '4');
INSERT INTO `ly_res_user` VALUES ('30', '4');
INSERT INTO `ly_res_user` VALUES ('31', '4');
INSERT INTO `ly_res_user` VALUES ('32', '4');
INSERT INTO `ly_res_user` VALUES ('33', '4');
INSERT INTO `ly_res_user` VALUES ('34', '4');
INSERT INTO `ly_res_user` VALUES ('35', '4');
INSERT INTO `ly_res_user` VALUES ('36', '4');
INSERT INTO `ly_res_user` VALUES ('37', '4');
INSERT INTO `ly_res_user` VALUES ('1', '6');
INSERT INTO `ly_res_user` VALUES ('2', '6');
INSERT INTO `ly_res_user` VALUES ('3', '6');
INSERT INTO `ly_res_user` VALUES ('5', '6');
INSERT INTO `ly_res_user` VALUES ('6', '6');
INSERT INTO `ly_res_user` VALUES ('7', '6');
INSERT INTO `ly_res_user` VALUES ('8', '6');
INSERT INTO `ly_res_user` VALUES ('34', '6');

-- ----------------------------
-- Table structure for ly_role
-- ----------------------------
DROP TABLE IF EXISTS `ly_role`;
CREATE TABLE `ly_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `state` varchar(3) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `roleKey` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_role
-- ----------------------------
INSERT INTO `ly_role` VALUES ('1', '0', '管理员', 'admin', '管理员');
INSERT INTO `ly_role` VALUES ('2', '0', '普通角色', 'simple', '普通角色');
INSERT INTO `ly_role` VALUES ('3', '0', '超级管理员', 'SUPER', '超级管理员');

-- ----------------------------
-- Table structure for ly_server_info
-- ----------------------------
DROP TABLE IF EXISTS `ly_server_info`;
CREATE TABLE `ly_server_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cpuUsage` varchar(255) DEFAULT NULL,
  `setCpuUsage` varchar(255) DEFAULT NULL,
  `jvmUsage` varchar(255) DEFAULT NULL,
  `setJvmUsage` varchar(255) DEFAULT NULL,
  `ramUsage` varchar(255) DEFAULT NULL,
  `setRamUsage` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `operTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_server_info
-- ----------------------------
INSERT INTO `ly_server_info` VALUES ('5', '18', '40', '49', '40', '71', '40', '121261494@qq.com', '2015-04-25 18:07:02', '<font color=\"red\">JVM当前：49%,超出预设值 40%<br>内存当前：71%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('6', '3', '40', '50', '40', '71', '40', '121261494@qq.com', '2015-04-25 18:08:03', '<font color=\"red\">JVM当前：50%,超出预设值 40%<br>内存当前：71%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('7', '5', '40', '50', '40', '70', '40', '121261494@qq.com', '2015-04-25 18:09:02', '<font color=\"red\">JVM当前：50%,超出预设值 40%<br>内存当前：70%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('8', '5', '40', '52', '40', '69', '40', '121261494@qq.com', '2015-04-25 18:10:03', '<font color=\"red\">JVM当前：52%,超出预设值 40%<br>内存当前：69%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('9', '2', '40', '52', '40', '68', '40', '121261494@qq.com', '2015-04-25 18:11:02', '<font color=\"red\">JVM当前：52%,超出预设值 40%<br>内存当前：68%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('10', '7', '40', '53', '40', '67', '40', '121261494@qq.com', '2015-04-25 18:12:02', '<font color=\"red\">JVM当前：53%,超出预设值 40%<br>内存当前：67%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('11', '5', '40', '54', '40', '67', '40', '121261494@qq.com', '2015-04-25 18:13:02', '<font color=\"red\">JVM当前：54%,超出预设值 40%<br>内存当前：67%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('12', '16', '40', '55', '40', '66', '40', '121261494@qq.com', '2015-04-25 18:14:02', '<font color=\"red\">JVM当前：55%,超出预设值 40%<br>内存当前：66%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('13', '5', '40', '56', '40', '65', '40', '121261494@qq.com', '2015-04-25 18:15:02', '<font color=\"red\">JVM当前：56%,超出预设值 40%<br>内存当前：65%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('14', '8', '40', '57', '40', '64', '40', '121261494@qq.com', '2015-04-25 18:16:02', '<font color=\"red\">JVM当前：57%,超出预设值 40%<br>内存当前：64%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('15', '3', '40', '58', '40', '63', '40', '121261494@qq.com', '2015-04-25 18:17:02', '<font color=\"red\">JVM当前：58%,超出预设值 40%<br>内存当前：63%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('16', '6', '40', '59', '40', '62', '40', '121261494@qq.com', '2015-04-25 18:18:03', '<font color=\"red\">JVM当前：59%,超出预设值 40%<br>内存当前：62%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('17', '1', '40', '60', '40', '61', '40', '121261494@qq.com', '2015-04-25 18:19:02', '<font color=\"red\">JVM当前：60%,超出预设值 40%<br>内存当前：61%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('18', '5', '40', '61', '40', '61', '40', '121261494@qq.com', '2015-04-25 18:20:02', '<font color=\"red\">JVM当前：61%,超出预设值 40%<br>内存当前：61%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('19', '5', '40', '38', '40', '61', '40', '121261494@qq.com', '2015-04-25 18:21:02', '<font color=\"red\">内存当前：61%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('20', '5', '40', '39', '40', '60', '40', '121261494@qq.com', '2015-04-25 18:22:02', '<font color=\"red\">内存当前：60%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('21', '4', '40', '40', '40', '59', '40', '121261494@qq.com', '2015-04-25 18:23:02', '<font color=\"red\">内存当前：59%,超出预设值  40%</font>');
INSERT INTO `ly_server_info` VALUES ('22', '32', '80', '41', '80', '81', '80', '121261494@qq.com', '2015-04-26 01:43:05', '<font color=\"red\">内存当前：81%,超出预设值  80%</font>');
INSERT INTO `ly_server_info` VALUES ('23', '55', '80', '55', '80', '81', '80', '121261494@qq.com', '2015-04-26 01:50:03', '<font color=\"red\">内存当前：81%,超出预设值  80%</font>');
INSERT INTO `ly_server_info` VALUES ('24', '13', '80', '53', '80', '81', '80', '121261494@qq.com', '2015-04-26 01:59:08', '<font color=\"red\">内存当前：81%,超出预设值  80%</font>');
INSERT INTO `ly_server_info` VALUES ('25', '85', '80', '58', '80', '72', '80', '121261494@qq.com', '2015-04-26 02:46:06', '<font color=\"red\">CPU当前：85%,超出预设值  80%<br></font>');
INSERT INTO `ly_server_info` VALUES ('26', '34', '80', '59', '80', '81', '80', '121261494@qq.com', '2015-04-27 00:29:06', '<font color=\"red\">内存当前：81%,超出预设值  80%</font>');
INSERT INTO `ly_server_info` VALUES ('27', '92', '80', '47', '80', '64', '80', '121261494@qq.com', '2015-04-27 00:44:07', '<font color=\"red\">CPU当前：92%,超出预设值  80%<br></font>');
INSERT INTO `ly_server_info` VALUES ('28', '85', '80', '49', '80', '68', '80', '121261494@qq.com', '2015-04-27 23:38:04', '<font color=\"red\">CPU当前：85%,超出预设值  80%<br></font>');
INSERT INTO `ly_server_info` VALUES ('29', '94', '80', '69', '80', '73', '80', '121261494@qq.com', '2015-04-28 01:35:03', '<font color=\"red\">CPU当前：94%,超出预设值  80%<br></font>');
INSERT INTO `ly_server_info` VALUES ('30', '6', '80', '43', '80', '87', '80', '121261494@qq.com', '2015-05-09 00:00:08', '<font color=\"red\">内存当前：87%,超出预设值  80%</font>');
INSERT INTO `ly_server_info` VALUES ('31', '88', '80', '59', '80', '87', '80', '121261494@qq.com', '2015-05-09 00:01:14', '<font color=\"red\">CPU当前：88%,超出预设值  80%<br>内存当前：87%,超出预设值  80%</font>');

-- ----------------------------
-- Table structure for ly_user
-- ----------------------------
DROP TABLE IF EXISTS `ly_user`;
CREATE TABLE `ly_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usernames` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `credentialsSalt` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `locked` varchar(3) DEFAULT '0',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletestatus` int(1) DEFAULT '0' COMMENT '逻辑删除状态0:存在1:删除',
  `salt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_user
-- ----------------------------
INSERT INTO `ly_user` VALUES ('1', '测试', 'simple', '78e21a6eb88529eab722793a448ed394', '4157c3feef4a6ed91b2c28cf4392f2d1', '0', '1', '2015-09-15 15:08:08', '0', null);
INSERT INTO `ly_user` VALUES ('2', '超级管理员', 'ROOT', '5f4dcc3b5aa765d61d8327deb882cf99', '4157c3feef4a6ed91b2c28cf4392f2d1', '0000', '1', '2015-10-13 09:59:03', '0', null);
INSERT INTO `ly_user` VALUES ('3', '管理员', 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', '4157c3feef4a6ed91b2c28cf4392f2d1', '3434', '0', '2015-10-08 10:40:14', '0', null);
INSERT INTO `ly_user` VALUES ('4', '周全备', 'zqb', '5f4dcc3b5aa765d61d8327deb882cf99', '2b92cf81b71382c82bb9ff143ecb6c4e', '开发人员', '1', '2015-10-13 09:58:56', '0', null);
INSERT INTO `ly_user` VALUES ('6', '陈云', 'cy1', '5f4dcc3b5aa765d61d8327deb882cf99', '09cba977da0ffb8e60ca79fce311f12f', 'dfdfd', '0', '2015-10-13 09:59:01', '0', null);

-- ----------------------------
-- Table structure for ly_userlogin
-- ----------------------------
DROP TABLE IF EXISTS `ly_userlogin`;
CREATE TABLE `ly_userlogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `loginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `loginIP` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ly_user_loginlist` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_userlogin
-- ----------------------------
INSERT INTO `ly_userlogin` VALUES ('143', '3', 'admin', '2015-09-14 19:23:32', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('144', '3', 'admin', '2015-09-14 19:43:38', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('145', '3', 'admin', '2015-09-14 19:43:54', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('146', '3', 'admin', '2015-09-14 19:49:07', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('147', '3', 'admin', '2015-09-14 20:00:05', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('148', '3', 'admin', '2015-09-15 09:44:14', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('149', '3', 'admin', '2015-09-15 09:55:57', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('150', '3', 'admin', '2015-09-15 09:56:30', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('151', '3', 'admin', '2015-09-15 09:57:11', '192.168.200.40');
INSERT INTO `ly_userlogin` VALUES ('152', '3', 'admin', '2015-09-15 09:58:00', '192.168.20.69');
INSERT INTO `ly_userlogin` VALUES ('153', '3', 'admin', '2015-09-15 09:58:19', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('154', '3', 'admin', '2015-09-15 10:11:03', '192.168.200.40');
INSERT INTO `ly_userlogin` VALUES ('155', '3', 'admin', '2015-09-15 11:41:25', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('156', '6', 'cy1', '2015-09-15 11:46:18', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('157', '3', 'admin', '2015-09-15 11:46:31', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('158', '6', 'cy1', '2015-09-15 11:47:04', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('159', '3', 'admin', '2015-09-15 11:50:55', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('160', '3', 'admin', '2015-09-15 15:07:51', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('161', '3', 'admin', '2015-09-15 17:06:18', '192.168.20.72');
INSERT INTO `ly_userlogin` VALUES ('162', '3', 'admin', '2015-09-15 17:34:01', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('163', '3', 'admin', '2015-09-15 18:02:44', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('164', '3', 'admin', '2015-09-15 18:09:54', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('165', '3', 'admin', '2015-09-16 09:11:55', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('166', '3', 'admin', '2015-09-16 10:38:21', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('167', '3', 'admin', '2015-09-17 11:29:04', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('168', '3', 'admin', '2015-09-17 16:22:13', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('169', '3', 'admin', '2015-09-17 16:25:31', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('170', '3', 'admin', '2015-09-17 16:36:27', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('171', '3', 'admin', '2015-09-17 16:36:41', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('172', '3', 'admin', '2015-09-17 17:44:55', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('173', '3', 'admin', '2015-09-18 15:03:48', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('174', '3', 'admin', '2015-09-21 09:29:25', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('175', '3', 'admin', '2015-09-21 14:57:38', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('176', '3', 'admin', '2015-09-21 14:58:17', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('177', '3', 'admin', '2015-09-21 17:47:37', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('178', '3', 'admin', '2015-09-22 11:28:30', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('179', '3', 'admin', '2015-09-22 11:40:09', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('180', '3', 'admin', '2015-09-22 12:27:17', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('181', '3', 'admin', '2015-09-22 17:29:01', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('182', '3', 'admin', '2015-09-22 17:36:51', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('183', '6', 'cy1', '2015-09-22 17:38:27', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('184', '6', 'cy1', '2015-09-22 17:38:51', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('185', '3', 'admin', '2015-09-22 17:41:16', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('186', '3', 'admin', '2015-09-24 09:31:23', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('187', '3', 'admin', '2015-09-24 15:36:06', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('188', '3', 'admin', '2015-09-24 17:36:41', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('189', '3', 'admin', '2015-09-25 09:31:00', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('190', '5', 'zhuchao', '2015-09-25 09:33:39', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('191', '3', 'admin', '2015-09-25 09:35:52', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('192', '3', 'admin', '2015-09-25 13:37:36', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('193', '5', 'zhuchao', '2015-09-25 13:38:55', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('194', '3', 'admin', '2015-10-08 09:17:14', '0:0:0:0:0:0:0:1');
INSERT INTO `ly_userlogin` VALUES ('195', '3', 'admin', '2015-10-08 09:19:04', '0:0:0:0:0:0:0:1');

-- ----------------------------
-- Table structure for ly_user_copy
-- ----------------------------
DROP TABLE IF EXISTS `ly_user_copy`;
CREATE TABLE `ly_user_copy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usernames` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `credentialsSalt` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `locked` varchar(3) DEFAULT '0',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletestatus` int(1) DEFAULT '0' COMMENT '逻辑删除状态0:存在1:删除',
  `salt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_user_copy
-- ----------------------------
INSERT INTO `ly_user_copy` VALUES ('1', '测试', 'simple', '78e21a6eb88529eab722793a448ed394', '4157c3feef4a6ed91b2c28cf4392f2d1', '0', '1', '2015-09-15 15:08:08', '0', null);
INSERT INTO `ly_user_copy` VALUES ('2', '超级管理员', 'ROOT', '5f4dcc3b5aa765d61d8327deb882cf99', '4157c3feef4a6ed91b2c28cf4392f2d1', '0000', '1', '2015-10-13 09:59:03', '0', null);
INSERT INTO `ly_user_copy` VALUES ('3', '管理员', 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', '4157c3feef4a6ed91b2c28cf4392f2d1', '3434', '0', '2015-10-08 10:40:14', '0', null);
INSERT INTO `ly_user_copy` VALUES ('4', '周全备', 'zqb', '5f4dcc3b5aa765d61d8327deb882cf99', '2b92cf81b71382c82bb9ff143ecb6c4e', '开发人员', '1', '2015-10-13 09:58:56', '0', null);
INSERT INTO `ly_user_copy` VALUES ('6', '陈云', 'cy1', '5f4dcc3b5aa765d61d8327deb882cf99', '09cba977da0ffb8e60ca79fce311f12f', 'dfdfd', '0', '2015-10-13 09:59:01', '0', null);

-- ----------------------------
-- Table structure for ly_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ly_user_role`;
CREATE TABLE `ly_user_role` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ly_user_role
-- ----------------------------
INSERT INTO `ly_user_role` VALUES ('1', '1');
INSERT INTO `ly_user_role` VALUES ('2', '3');
INSERT INTO `ly_user_role` VALUES ('3', '1');
INSERT INTO `ly_user_role` VALUES ('4', '1');
INSERT INTO `ly_user_role` VALUES ('6', '3');

-- ----------------------------
-- Table structure for oauth2_client
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_client`;
CREATE TABLE `oauth2_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) DEFAULT NULL,
  `client_id` varchar(100) DEFAULT NULL,
  `client_secret` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_oauth2_client_client_id` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth2_client
-- ----------------------------
INSERT INTO `oauth2_client` VALUES ('1', 'chapter17-client', 'c1ebe466-1cdc-4bd3-ab69-77c3561b9dee', 'd8346ea2-6017-43ed-ad68-19c0f971738b');

-- ----------------------------
-- Table structure for oauth2_user
-- ----------------------------
DROP TABLE IF EXISTS `oauth2_user`;
CREATE TABLE `oauth2_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `salt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_oauth2_user_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth2_user
-- ----------------------------
INSERT INTO `oauth2_user` VALUES ('1', 'admin', 'a6784fb6fed85b51b15b85708f96db53', 'a71cf39028e78eea4e1b6b15ba6949e3');
