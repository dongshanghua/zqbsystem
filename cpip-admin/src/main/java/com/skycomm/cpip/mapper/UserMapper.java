package com.skycomm.cpip.mapper;

import java.util.List;

import com.skycomm.cpip.entity.UserFormMap;
import com.skycomm.cpip.mapper.base.BaseMapper;


public interface UserMapper extends BaseMapper {

	public List<UserFormMap> findUserPage(UserFormMap userFormMap);
	
	
	public List<UserFormMap> seletUser(UserFormMap userFormMap);
}
