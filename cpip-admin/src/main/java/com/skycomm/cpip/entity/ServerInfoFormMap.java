package com.skycomm.cpip.entity;

import com.skycomm.cpip.annotation.TableSeg;
import com.skycomm.cpip.util.FormMap;

@TableSeg(tableName = "ly_server_info", id="id")
public class ServerInfoFormMap extends FormMap<String, Object> {
	private static final long serialVersionUID = 1L;
}